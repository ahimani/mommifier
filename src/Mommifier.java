import java.util.Arrays;
import java.util.List;

//Understands as modify strings containing more than 30% vowels.
class Mommifier {
    private List<Character> vowel = Arrays.asList('a','e','i','o','u');
    String mommify(String input) {
          if(containsLessThanOrEqualThirtyPercentVowel(input)) {
            return input;
        }
        return insertMommys(input);
    }

    private String insertMommys(String input) {
        StringBuilder builder = new StringBuilder();
        boolean flag = false;
        for(char eachChar : input.toCharArray()){
            if (vowel.contains(eachChar)){
                if (!flag) {
                    builder.append("mommy");
                }
                flag = true;
            }
            else {
                flag = false;
                builder.append(eachChar);
            }
        }

        return builder.toString();
    }

    boolean containsLessThanOrEqualThirtyPercentVowel(String input) {
        float countOfVowels = 0;
        for (char eachChar : input.toCharArray()) {
            if (vowel.contains(eachChar)) {
                countOfVowels++;
            }
        }
        return input.equals("") || countOfVowels / input.length() <= 0.3;
    }


}
