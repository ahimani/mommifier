import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class MommifierTest {

    private Mommifier mommifier;

    @Before
    public void setUp() throws Exception {
        mommifier = new Mommifier();
    }

    @Test
    public void shouldReturnEmptyStringWhenMommifyingEmptyString() throws Exception {
        assertEquals("", mommifier.mommify(""));
    }

    @Test
    public void shouldReturnSameStringWhenMommifyingSingleConsonant() throws Exception {
        assertEquals("c", mommifier.mommify("c"));
    }

    @Test
    public void shouldReturnMommyWhenMommifyingSingleVowel() throws Exception {
        assertEquals("mommy", mommifier.mommify("a"));
    }

    @Test
    public void shouldNotMommifyWhenStringIsHard() throws Exception {
        assertEquals("hard", mommifier.mommify("hard"));
    }

    @Test
    public void shouldMommifyWhenStringIsHis() throws Exception {
        assertEquals("hmommys", mommifier.mommify("his"));
    }

    @Test
    public void shouldMommifyWhenStringIsHear() throws Exception {
        assertEquals("hmommyr", mommifier.mommify("hear"));
    }

    @Test
    public void shouldMommifyWhenStringIsHeaaaa() throws Exception {
        assertEquals("hmommy", mommifier.mommify("heaaaa"));
    }

    @Test
    public void shouldMommifyWhenStringIsHere() throws Exception {
        assertEquals("hmommyrmommy", mommifier.mommify("here"));
    }

    @Test
    public void shouldReturnTrueWhenPercentageOfVowelIsLessThanThirtyIfStringIsHerd(){
        assertTrue(mommifier.containsLessThanOrEqualThirtyPercentVowel("herd"));
    }

    @Test
    public void shouldReturnFalseWhenPercentageOfVowelIsGreaterThanThirtyIfStringIsHere(){
        assertFalse(mommifier.containsLessThanOrEqualThirtyPercentVowel("here"));
    }








}
